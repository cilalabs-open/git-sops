FROM google/cloud-sdk:alpine
RUN apk add curl && \
    curl -L https://github.com/mozilla/sops/releases/download/v3.6.1/sops-v3.6.1.linux -o /usr/local/bin/sops && \
    chmod +x /usr/local/bin/sops
RUN apk del curl
ENTRYPOINT ["/bin/sh"]